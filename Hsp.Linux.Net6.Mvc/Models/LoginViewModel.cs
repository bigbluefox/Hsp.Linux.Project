﻿using System.ComponentModel.DataAnnotations;

namespace Hsp.Linux.Net6.Mvc.Models
{
    /// <summary>
    /// 登录视图实体
    /// </summary>
    public class LoginViewModel
    {
        //[Required]
        [Display(Name = "电子邮件")]
        [EmailAddress]
        public string? Email { get; set; }

        [Required(ErrorMessage = "{0}不能为空")]
        [Display(Name = "手机号码")]
        //[Phone]
        //[DataType(DataType.PhoneNumber)]
        public string? PhoneNumber { get; set; }

        [Required(ErrorMessage = "{0}不能为空")]
        [DataType(DataType.Password)]
        [Display(Name = "密码")]
        public string? Password { get; set; }

        [Required(ErrorMessage = "{0}不能为空")]
        [Display(Name = "验证码")]
        public string? VerifyCode { get; set; }

        [Display(Name = "记住我?")]
        public bool RememberMe { get; set; }

        /// <summary>
        /// 返回消息
        /// </summary>
        public string? Message { get; set; }
    }
}
