﻿using Hsp.Linux.Net6.Mvc.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Drawing;
using System.Drawing.Imaging;

namespace Hsp.Linux.Net6.Mvc.Controllers
{
    public class AccountController : Controller
    {
        /// <summary>
        /// 验证码
        /// Auth/VerifyCode
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult VerifyCode()
        {
            //string code = "", cacheKey = "";
            Bitmap bitmap = VerifyCodeHelper.CreateVerifyCode(out string code);
            base.HttpContext.Session.SetString("CheckCode", code);

            string verifyCode = base.HttpContext.Session.GetString("CheckCode")!;

            MemoryStream stream = new MemoryStream();
            bitmap.Save(stream, ImageFormat.Gif);
            return File(stream.ToArray(), "image/gif");
        }

        /// <summary>
        /// 登录视图
        /// Auth/Login
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }
    }
}
