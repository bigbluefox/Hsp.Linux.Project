var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

#region 注册使用Session

// Nuget 安装 Microsoft.AspNetCore.Session
// Microsoft.AspNetCore.Http.Extensions

builder.Services.AddSession(options =>
{
    // Set a short timeout for easy testing.
    options.IdleTimeout = TimeSpan.FromSeconds(600);
    // You might want to only set the application cookies over a secure connection:
    options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
    options.Cookie.SameSite = SameSiteMode.Strict;
    options.Cookie.HttpOnly = true;
    // Make the session cookie essential
    options.Cookie.IsEssential = true;
});

#endregion

#region 登录验证

//builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
//.AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
//{
//    // Make the session cookie essential
//    options.Cookie.IsEssential = true;

//    // Cookie settings
//    options.Cookie.HttpOnly = true;
//    options.ExpireTimeSpan = TimeSpan.FromMinutes(300);
//    options.Cookie.Name = "COOKIE_HSP_CURRENT_USER";
//    options.LoginPath = new PathString("/Auth/Login");
//    options.LogoutPath = new PathString("/Auth/Logout");
//    options.AccessDeniedPath = new PathString("/Error/Forbidden");
//});

#endregion

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseSession();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
