﻿CentOS8下使用docker 安装redis

1、拉取 redis镜像

docker pull redis

Status: Downloaded newer image for redis:latest
docker.io/library/redis:latest

2、运行容器

docker run --name redis -p 6379:6379 -d --restart=always redis redis-server --appendonly yes --requirepass "2zeumcto47f2g9kpzhdh2"

输出：617f7b7070db75f20513631d65b12910c50c4082bfdc2c6ae8e51f50642441b3

这个长字符串叫做容器 ID，对每个容器来说都是唯一的，我们可以通过容器 ID 来查看对应的容器发生了什么。

--name为容器取一个唯一的名字

-p端口映射，把宿主机的端口映射到容器内的端口

--restar=always随容器启动而启动

redis-server --appendonly yes在容器里执行redis-server命令，打开redis持久化

--requirepass密码

3、进入容器redis

docker exec -it redis bash

docker exec -it 617f7b7070db redis-cli

docker exec -it redis redis-cli

auth 2zeumcto47f2g9kpzhdh2（密码）
