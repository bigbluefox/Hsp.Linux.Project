CentOS8打开对外开放端口

1:查看firewall防火墙状态
firewall-cmd --state
或者
systemctl status firewalld
 
2：打开防火墙
systemctl start firewalld
 
3：关闭防火墙
ystemctl stop firewalld
 
4：重启防火墙
firewall-cmd --reload
或者
systemctl reload firewalld
 
5：开机自启动防火墙
systemctl enable firewalld
 
6：禁止开机启动防火墙
systemctl disable firewalld
 
7：查看已打开的端口
firewall-cmd --list-ports
 
8：打开端口
firewall-cmd --permanent --zone=public --add-port=8080/tcp
其中permanent表示永久生效，public表示作用域，8080/tcp表示端口和类型
 
9:关闭端口
firewall-cmd --permanent --zone=public --remove-port=8080/tcp

注意：打开或关闭端口要重启防火墙才生效


查看所有已开放端口

firewall-cmd --zone=public --list-ports

查看 8080 端口是否开放：

firewall-cmd --zone=public --query-port=8080/tcp











