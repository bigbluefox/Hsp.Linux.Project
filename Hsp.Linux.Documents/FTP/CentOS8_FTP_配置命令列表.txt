FTP_CentOS8



https://www.cnblogs.com/zhi-leaf/p/5983550.html

1.查看是否已经安装vsftpd
vsftpd –v

2. yum安装vsftpd
yum -y install vsftpd

3. 安装完成后,查看位置
whereis vsftpd

4. 直接启动VSFTP服务
systemctl start vsftpd.service
systemctl status vsftpd.service

5. 查看是否启动成功
netstat -npal|grep vsftpd

  保证下面3项为YES

    anonymous_enable=YES

    anon_upload_enable=YES

    anon_mkdir_write_enable=YES


6. 关闭SELinux限制，添加防火墙白名单
getsebool -a | grep ftp

7. 将ftp加入防火墙白名单
firewall-cmd --permanent --zone=public --add-service=ftp
firewall-cmd --reload

8. 查看防火墙状态
firewall-cmd --list-all

9. 创建宿主用户
cd /home
mkdir vsftpd

# 创建用户 ftpuser 指定 `/home/vsftpd` 目录
/usr/sbin/useradd -g root -M -d /home/vsftpd ftpuser

chmod 777 vsftpd

# 设置用户 ftpuser 的密码：5b*
passwd ftpuser
# 把 /home/vsftpd 的所有权给ftpuser.root
chown -R ftpuser.root /home/vsftpd

10.重启VSFTP服务
systemctl restart vsftpd.service

1.3、设置vsftpd开机启动

systemctl enable vsftpd.service

11. 可以在windows中访问测试，或者使用FileZilla连接

Microsoft Windows [版本 10.0.18363.900]
(c) 2019 Microsoft Corporation。保留所有权利。

C:\Users\lihai>ftp 192.168.81.134
连接到 192.168.81.134。
220 (vsFTPd 3.0.3)
200 Always in UTF8 mode.
用户(192.168.81.134:(none)): ftpuser
331 Please specify the password.
密码:
230 Login successful.
ftp>










关闭SELinux

vi /etc/selinux/config
将 SELINUX=XXX -->XXX 代表级别
改为
SELINUX=disabled
重启系统reboot
CentOS7下查看vsftpd服务的状态
systemctl status vsftpd.service
设置开机启动
systemctl enable vsftpd.service

chmod 777 把文件/目录修改为可读可写可执行

chmod 777 新建的文件夹路径


问题记录：
访问ftp服务器提示没有权限访问，ftp服务器没有完全开放权限

解决方法：

passwd 用户名（更改用户密码可选）
chown -R ftpbrk:root /home/ftp/ftpdir
chmod 777 /home/ftp/ftpdir/
/bin/systemctl restart vsftpd.service （重启可选）
查看进程状态：

/bin/systemctl status vsftpd.service


https://www.cnblogs.com/zhi-leaf/p/5983550.html
