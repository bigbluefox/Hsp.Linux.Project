https://blog.csdn.net/our_times/article/details/98882701
https://www.cnblogs.com/yaowen/p/9486138.html


1．配置Mysql 8.0安装源
sudo rpm -Uvh https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm

https://dev.mysql.com/get/mysql80-community-release-el7-4.noarch.rpm


2．安装Mysql 8.0
sudo yum --enablerepo=mysql80-community install mysql-community-server


3. 使用 yum安装mysql
yum repolist all | grep mysql

1.2.5 安装mysql 命令如下：
yum install mysql-community-server


1.2.6 开启mysql 服务
systemctl start mysqld.service
1.2.7 获取初始密码登录mysql
mysql在安装后会创建一个root@locahost账户，并且把初始的密码放到了/var/log/mysqld.log文件中；

cat /var/log/mysqld.log | grep password

[root@localhost yum.repos.d]# cat /var/log/mysqld.log | grep password
2021-12-12T17:33:59.370142Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: kT#.sCT7mkp;

mysql -u root -p
mysql>use mysql;
mysql>select 'host' from user where user='root';
mysql>update user set host = '%' where user ='root';
mysql>flush privileges;
mysql>select 'host'   from user where user='root';



设置完成后输入exit退出mysql，回到终端shell界面，接着开启系统防火墙的3306端口：

sudo firewall-cmd --add-port=3306/tcp --permanent
sudo firewall-cmd --reload


关闭MySQL主机查询dns#
MySQL会反向解析远程连接地址的dns记录，如果MySQL主机无法连接外网，则dns可能无法解析成功，导致第一次连接MySQL速度很慢，所以在配置中可以关闭该功能。
参考文档
打开/etc/my.cnf文件，添加以下配置：

[mysqld]
skip-name-resolve
重启服务#
sudo systemctl restart mysqld
本机测试安装后，MySQL8.0默认已经是utf8mb4字符集，所以字符集不再修改



错误号码2058 Plugin caching_sha2_password could not be loaded:


ALTER USER'root'@'%' IDENTIFIED WITH mysql_native_password BY 'Root@123';

flush privileges;






