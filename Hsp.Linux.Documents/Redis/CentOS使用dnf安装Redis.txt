CentOS使用dnf安装Redis

太复杂了，不过最后安装成功了。

1.查询可用的redis安装包
输入以下命令：

dnf list redis
输出：

redis.x86_64                                                                           3.2.10-2.el7 
2.安装软件
输入以下命令：

dnf install redis
3.检查是否安装成功：
输入：

redis-server
如果能成功启动redis安装成功。


添加开机启动

$ sudo systemctl enable redis.service 

开始运行

$ sudo systemctl start redis.service 

配置文件在/etc/redis.conf下

各位根据自己需求自行配置即可。

redis日志会在/var/log/redis/redis.log下

查看日志有警告，这是不能接受的！

显示有几条警告，并且Reids也给出了一些解决方法：

第一个警告：TCP backlog是TCP全连接状态队列大小，Centos8默认值是128，cat /proc/sys/net/core/somaxconn可查看，Redis配置文件配置了511的值，超过了限制，因此会出现警告。解决方法：

$ echo 'net.core.somaxconn = 512' | sudo tee -a /etc/sysctl.conf > /dev/null


使其生效： 

$ sudo sysctl -p
第二个警告：

overcommit_memory是内存分配策略

0， 表示内核将检查是否有足够的可用内存供应用进程使用；如果有足够的可用内存，内存申请允许；否则，内存申请失败，并把错误返回给应用进程。
1， 表示内核允许分配所有的物理内存，而不管当前的内存状态如何。
2， 表示内核允许分配超过所有物理内存和交换空间总和的内存

Centos8默认值0，需要设置为1，详细的这里不再描述，各位可以自行搜索。

$ echo 'vm.overcommit_memory = 1' | sudo tee -a /etc/sysctl.conf > /dev/null
使其生效：  

$ sudo sysctl -p


第三个警告（关于Transparent HugePages，各位请自行搜索，在此不提）提示中已经写的很清楚：

echo madvise > /sys/kernel/mm/transparent_hugepage/enabled
不过我打开rc.local时发现一句话：

强烈建议使用自己的服务而不使用该文件，囧

创建文件

$ sudo vim /usr/bin/disable-transparent-hugepage
输入： 

#!/bin/bash
echo madvise > /sys/kernel/mm/transparent_hugepage/enabled


运行： 

$ sudo chown root:root /usr/bin/disable-transparent-hugepage
$ sudo chmod 770 /usr/bin/disable-transparent-hugepage

sudo vim /etc/systemd/system/disable-transparent-hugepage.service

输入：

[Unit]
Description=Disable Transparent-Huge-Pages for Redis.
Before=redis.service
 
[Service]
Type=exec
ExecStart=/usr/bin/disable-transparent-hugepage
 
[Install]
WantedBy=multi-user.target

 使其开机运行：

$ sudo systemctl enable disable-transparent-hugepage.service

最后别忘了重启Redis服务

sudo systemctl restart redis.service


firewall-cmd --zone=public --add-port=6379/tcp --permanent

