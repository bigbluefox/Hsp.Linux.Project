﻿using Hsp.Linux.Net6.Web.Models;
using Hsp.Linux.Net6.Web.Utility.Filter;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Hsp.Linux.Net6.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [AuthMenu(22)]
        public IActionResult Index()
        {
            return View();
        }

        [AuthMenu(Id = 22)]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}