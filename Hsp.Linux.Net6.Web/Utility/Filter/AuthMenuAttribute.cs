﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace Hsp.Linux.Net6.Web.Utility.Filter
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public class AuthMenuAttribute : Attribute, IActionFilter
    {
        public int Id { get; set; }

        public AuthMenuAttribute()
        {
        }

        public AuthMenuAttribute(int id)
        {
            this.Id = id;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            //throw new NotImplementedException();
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var id = this.Id;

            var abc = context;



        }
    }
}
